#include <stdexcept>
#include "Calculo.h"


unsigned int Calculo::calcularPorBaseAltura(unsigned int altura, unsigned int base) {


    if (base <= 0) {
        throw std::invalid_argument("El dato de la base ingresado es menor que 1.");
    } else if (altura <= 0) {
        throw std::invalid_argument("El dato de la altura ingresado es menor que 1.");
    }
    return (base * altura) / 2;

}

unsigned int Calculo::calcularIngresandoLados(unsigned int ladoCorto, unsigned int ladoLargo, unsigned int tercerLado) {


    if (ladoCorto <= 0) {
        throw std::invalid_argument("El lado mas corto es menor que 0.");
    } else if (ladoLargo <= 0) {
        throw std::invalid_argument("El lado mas largo es menor que 0.");
    } else if (tercerLado <= 0) {
        throw std::invalid_argument("El tercer lado es menor que 0.");

    } else if ((float) (sqrt((pow(ladoCorto, 2) + pow(ladoLargo, 2)))) <= (float) (tercerLado)) {
            throw std::invalid_argument("El triangulo no es cerrado.");
        }

    return (ladoCorto * ladoLargo) / 2;

}