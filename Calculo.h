
#ifndef AREA_TRIANGULO_CALCULO_H
#define AREA_TRIANGULO_CALCULO_H

using namespace std;

#include <cmath>

class Calculo {

private:

public:

    unsigned int calcularPorBaseAltura(unsigned int altura, unsigned int base);

    unsigned int calcularIngresandoLados(unsigned int ladoCorto, unsigned int ladoLargo, unsigned int tercerLado);
};


#endif //AREA_TRIANGULO_CALCULO_H
