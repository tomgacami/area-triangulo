#include <iostream>
#include "Calculo.h"

using namespace std;


// Exception Handler
void handleExceptions(const string &eWhat) {
    cout << "       " << eWhat << endl;
}

int main() {

    unsigned int opcion, resultado = 0;
    char opcionSalida = 'a';
    Calculo *cal = new(Calculo);

    cout << "BUENAS BUENAS JUGADOR" << endl;

    do {
        cout << "Ingrese la opción" << endl;
        cout << "   1-Para ingresar base y altura" << endl;
        cout << "   2-Para ingesar la medida de los lados del triangulo" << endl << endl;
        cout << "Ingrese opcion: ";
        cin >> opcion;
        cin.clear();

        switch (opcion) {
            case 1: {
                unsigned int altura = 0, base = 0;

                cout << "Ingrese la medida de los lados de triangulo:" << endl;

                cout << "   Altura: ";
                cin >> altura;
                cin.clear();
                cout << "  Base: ";
                cin >> base;
                cin.clear();

                try {
                    resultado = cal->calcularPorBaseAltura(altura, base);
                    cout << endl << "El area del trangulo es: " << resultado;
                } catch (invalid_argument &except) {
                    handleExceptions(except.what());
                }
                break;
            }
            case 2: {
                unsigned int ladoLargo, ladoCorto, tercerLado;

                cout << "Ingrese el lado mas largo: ";
                cin >> ladoLargo;
                cin.clear();
                cout << "Ingrese el lado mas corto: ";
                cin >> ladoCorto;
                cin.clear();
                cout << "Ingrese el tercerLado: ";
                cin >> tercerLado;
                cin.clear();

                try {
                    resultado = cal->calcularIngresandoLados(ladoCorto, ladoLargo, tercerLado);
                    cout << endl << "El area del trangulo es: " << resultado;
                } catch (invalid_argument &except) {
                    handleExceptions(except.what());
                }
                break;
            }
            default:
                cout << endl << "No hay opcion disponible para valor ingresado" << endl;
                cin.clear();
        }

        cout << endl << "Quiere seguir calculando? S/n: ";
        cin.ignore();
        cin >> opcionSalida;
        cin.clear();

        while (opcionSalida != 'S' && opcionSalida != 'n') {

            cout << "Opcion no valida, vuelva a ingresar opcion: ";
            cin >> opcionSalida;
            cin.clear();
        }


    } while (opcionSalida == 'S');

    cout << endl << endl << "FIN DEL PROGRAMA!!!" << endl << endl;
    return 0;
}